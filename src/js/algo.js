var ws = [];
for (var d = 0; d < D; ++d)
  ws[d] = Math.random();

while(true) {
  var js = []; // gradient of LMS
  for (var d = 0; d < D; ++d)	js[d] = 0;

  for (var n = 0; n < N; ++n) {
    var p = 0;
    for (var d = 0; d < D; ++d)
      p += samples[n][d] * ws[d];

    for (var d = 0; d < D; ++d)
      js[d] += (p - signs[n]) * samples[n][d];
  }

  // if js is small enough, exit the loop
  if (norm2(js) < 0.001) break;


  // update the weight vector
  for (var d = 0; d < D; ++d)
    ws[d] -= 0.01 * js[d];
}

console.log((- ws[1] / ws[2]) + " * x + " + (- ws[0] / ws[2]));

function norm2(vs) {
  var c = 0;
  for (var d = 0; d < D; ++d)	c += vs[d] * vs[d];
  return c;
}