import brain from 'brain';
import $ from 'jquery'
import learningData from '../inputs.json';
import {ConvertJsonToTable} from './json-to-html';
import _ from 'lodash';
//import {Classify} from "./binary-classifier";

const jsonHtmlTable = ConvertJsonToTable(learningData.items, 'jsonTable', null, 'Download');
let data = [];

$('#root').append(jsonHtmlTable);





$.each(learningData.items, (key, val) => {
    const last = val['output'];
    let item = {input: _.omit(val, ['output'])};

    if(last == 0)
    {
      item['output'] = {hardcore: 1}
    }
    else
    {
      item['output'] = {casual: 1}
    }
    data.push(item);
  });


//Classify(data);

let net = new brain.NeuralNetwork();
//
// let table = [
//   {input:[180, 77, 3, 6, 0, 100], output: [0]},
//   {input:[182, 88, 4, 4, 1, 50], output: [0]},
//   {input:[170, 55, 20, 2, 0, 20], output: [1]},
//   {input:[165, 57, 25, 1, 1, 10], output: [1]},
//   {input:[160, 53, 15, 3, 0, 0], output: [1]},
//   {input:[175,70, 7, 7, 1, 60], output: [0]}
// ];
//
//
net.train(data);



$('#classify').click( ()=> {
  let divinity = $('#divinity').val();
  let bgate = $('#bgate').val();
  let d2 = $('#d2').val();
  let lol = $('#lol').val();
  let overwatch = $('#overwatch').val();

  var output = net.run({
    "divinity": divinity,
    "baldurs_gate": bgate,
    "dota": d2,
    "lol": lol,
    "overwatch": overwatch
  });
  console.log(divinity);
  console.log(output);
  if(output['casual'] > output['hardcore'])
  {
    alert("Casual player");
  }
  else
  {
    alert("Hardcore player");
  }
});
