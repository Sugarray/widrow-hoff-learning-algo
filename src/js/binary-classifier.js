import _ from 'lodash';

const learnspeed = 0.9;
const epsilon = 0.01;

let outputs = [];

export function Classify(data) {

  let normalizedSamples = Normalize(data);
  let weights = _.fill(new Array(normalizedSamples[0].length), 0.01 * _.random(-1,1));

  while(1==1)
  {
    for(let i = 0; i < data.items.length; i++)
    {
      let item = _.take(data.items[i]);
      for(let j = 0; j < item.length; j++)
      {

      }
    }
  }

}

function Normalize(data)
{
  let normalizedData = [];
  let normalized = [];
  let samples = [];

  for(let i = 0; i< data.length; i++)
  {
    let sample = _.values(data[i].input);
    let output = _.values(data[i].output);

    outputs.push(output);
    samples.push(sample);
  }

  for(let i = 0; i < samples.length; i++)
  {
    for(let j = 0; j < samples[i].length; j++)
    {
      let column = samples.map( (value) => { return value[j]});
      normalized[j] = (column[i] - _.min(column)) / (_.max(column) - _.min(column));
    }
    normalizedData.push(normalized.slice());
  }

  return normalizedData;
}